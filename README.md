# About Me

I'm a Product/UX Designer with over 18 years of experience. Most recently, I worked at Pivotal. When I started there in 2014, I was one of two product designers working on the open source Pivotal/Cloud Foundry initiative. This initiative would eventually bring Pivotal to IPO in 2018. I grew the design practice at Pivotal/Cloud Foundry from two designers in San Francisco to 8+ designers, directly overseeing the recruitment, onboarding, and mentoring of new hires. In addition, I expanded the product design practice to other West Coast offices and managed the onboarding of new designers in these offices.

I have worked on a wide range of platforms and domains. I started my career as a multimedia designer, which landed me in the early stages of the "e-learning" space and at Enspire Learning. At Enspire, I played the roles of art director, interaction designer, instructional designer, and 2D/3D animator.

Early in my career, I wanted to make tools that people used in their daily lives. To this end, I joined McLane to lead the design of a logistics system used by military personnel in the theatre of war. The system facilitated the complete withdrawal of US troops from Iraq (2011). I also formed and managed a 6-member design practice. We worked on new business ventures in healthcare and civilian logistics.

As early as 2008, I began working on Agile teams and led initiatives to merge UX design into the Agile framework. This interest led me to ThoughtWorks where I worked with airlines, banks, and media companies to help them on their Agile adoption journey. While at ThoughtWorks, I delivered strategic web and mobile experiences for millions of customers. In the second half of my tenure there, I worked on enterprise tools that incorporated Agile principles into core features and key customer success metrics.

During my time in San Francisco, I started the Lean UX SF meetup group as a platform for designers to gather and share best practices. The group grew to 6,000 members within four years.

My interest in Lean and hypothesis-driven development attracted me to Neo Innovation where I advised Fortune 1000 companies on their Lean adoption journey. I led engagements that combined growth marketing and rapid prototyping to validate new business models.

After I had worked at Pivotal for nearly five years, I decided to take a sabbatical so that I could travel, write, and spend more time with family.

I enjoy working with vision-driven companies that make a positive impact in our world.

# Skills

* User-Centered Design
* Leadership
* Coaching
* Facilitation
* Lean UX
* Agile Planning
* HTML/CSS
* Ruby on Rails

# My Values

* Keep it simple
* Listen, then speak
* Be kind
* Be authentic

# My Ideal Design Process

* Explore the problem space
* Explore the solution space
* Converge at the last responsible moment

# GitLab Handle
[@mikelong](https://gitlab.com/mikelong)

